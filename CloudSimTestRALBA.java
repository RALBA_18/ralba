package org.cloudbus.cloudsim.examples;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.lists.CloudletList;
import org.cloudbus.cloudsim.lists.VmList;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

import sun.security.jca.GetInstance.Instance;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

/**
 * Our proposed technique that assign cloudlets to VMs as per their share of execution
 * 03 Vms with different MIPS requirements.
 */
@SuppressWarnings("unused")
public class CloudSimTestRALBA {

	/** The cloudlet list. */
	private static List<Cloudlet> cloudletList;
	private static Map<Vm, Double> vmMap ;
	private static Map<Vm,Double> vmShareMap;
	private static Map<Cloudlet,Vm> cloudlet2VmMap;

	private static int noVm, noCloudlet;
	private static List<Vm> vmList;

	private static Map<Vm,Double> createVM(int userId, int vms, int idShift) {
		//Creates a container to store VMs. This list is passed to the broker later
		Map<Vm, Double> vMap = new HashMap<Vm,Double>();

		//VM Parameters
		long size = 1000; //image size (MB)
		int ram = 512; //vm memory (MB)
		int mips = 500;
		long bw = 1000;
		int pesNumber = 1; //number of cpus
		String vmm = "Xen"; //VMM name

		//create VMs
		Vm[] vm = new Vm[vms]; 

		for(int i=0;i<vms;i++){			//same specs VMs
			switch(i%8)
			{
			case 0:
				vm[i] = new Vm(idShift + i, userId, mips-400, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;
			case 1:
				vm[i] = new Vm(idShift + i, userId, mips, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;
			case 2:
				vm[i] = new Vm(idShift + i, userId, mips+250, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;
			case 3:
				vm[i] = new Vm(idShift + i, userId, mips*2, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;			
			case 4:
				vm[i] = new Vm(idShift + i, userId, mips+750, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;
			case 5:
				vm[i] = new Vm(idShift + i, userId, mips*3, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;
			case 6:
				vm[i] = new Vm(idShift + i, userId, mips+1250, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;
			case 7:
				vm[i] = new Vm(idShift + i, userId, mips*8, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
				break;	
			}
			vMap.put(vm[i],0.0);
			}
				
		return vMap;
	}	
	private static Map<Vm,Double> createVM_LR(int userId, int vms, int idShift) {
		//Creates a container to store VMs. This list is passed to the broker later
		Map<Vm, Double> vMap = new HashMap<Vm,Double>();

		//VM Parameters
		long size = 1000; //image size (MB)
		int ram = 512; //vm memory (MB)
		int mips_s = 500;
		int mips_m = 1000;
		int mips_f = 2000;
		long bw = 1000;
		int pesNumber = 1; //number of cpus
		String vmm = "Xen"; //VMM name
		
		int vm_s = (int) (vms * 0.30);  
		int vm_f = (int) (vms * 0.35);
		int vm_m = vms - (vm_s + vm_f);
		
		//create VMs
		Vm[] vm = new Vm[vms]; 

		for(int i=0;i<vm_s;i++){
			vm[i] = new Vm(idShift + i, userId, mips_s, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
			vMap.put(vm[i],0.0);
		}
		for(int i=0;i<vm_m;i++){		
			vm[i] = new Vm(idShift + i, userId, mips_m, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
			vMap.put(vm[i],0.0);
		}
		for(int i=0;i<vm_f;i++){
			vm[i] = new Vm(idShift + i, userId, mips_f, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
			vMap.put(vm[i],0.0);
		}
		return vMap;
	}
	private static int newRandom(int min, int max)
	{
		int ranValue = 0;
		Random ran = new Random();
		ranValue = ran.nextInt(max-min) + min;
		return ranValue;
	}	
	public static Map<Cloudlet,Vm> createDataSet(int userId, int idShift) throws IOException
	{
		// Creates a container to store Cloudlets
		Map<Cloudlet,Vm> cvMap = new HashMap<Cloudlet,Vm>();
		
		//cloudlet parameters
		long fileSize = 300;
		long outputSize = 300;
		int pesNumber = 1;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		Cloudlet cloudlet;

	    FileReader in = new FileReader("E:/PhD CS/2. Research Work/Results and Working in CloudSim/Simulation Results/Dataset_LB_Monto_Carlo_1000.txt");
		//FileReader in = new FileReader("E:/RDataset20000.txt");
	    BufferedReader br = new BufferedReader(in);
	    long size = 0;
	    while (br.readLine() != null) {
	    	size = (long) Float.parseFloat(br.readLine());
	    	cloudlet = new Cloudlet(idShift++, size, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null); 
	    }
	    in.close();
	    return cvMap;
	}

	private static Map<Cloudlet,Vm> createCloudletRandom(int userId,int size, int idShift){
		// Creates a container to store Cloudlets
		Map<Cloudlet,Vm> cvMap = new HashMap<Cloudlet,Vm>();

		//cloudlet parameters
//		long lengthXS = 20, lengthS = 200, length = 1000, lengthL = 7500, lengthXL = 10000;
		long fileSize = 300;
		long outputSize = 300;
		int pesNumber = 1;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		Cloudlet cloudlet;
		int sXS, sS, sM, sL, sXL;
		sXS = (int) (size * 0.20);
		sS = (int) (size * 0.60);
		sM = (int) (size * 0.05);
		sL = (int) (size * 0.10);
		sXL = size - (sXS+sS+sM+sL);
		
		for(int i=0;i<sXS;i++){
			if (i%2==0)
				cloudlet = new Cloudlet(idShift++, newRandom(0,250), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			else
				cloudlet = new Cloudlet(idShift++, newRandom(0,250), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0;i<sS;i++){
			if (i%2==0)
				cloudlet = new Cloudlet(idShift++, newRandom(800,1200), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			else
				cloudlet = new Cloudlet(idShift++, newRandom(800,1200), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0;i<sM;i++){
			if (i%2==0)
				cloudlet = new Cloudlet(idShift++, newRandom(1800,2500), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			else
				cloudlet = new Cloudlet(idShift++, newRandom(1800,2500), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0;i<sL;i++){
			if (i%2==0)
				cloudlet = new Cloudlet(idShift++, newRandom(7000,10000) , pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			else
				cloudlet = new Cloudlet(idShift++, newRandom(7000,10000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sXL;i++){
			if (i%2==0)
				cloudlet = new Cloudlet(idShift++, newRandom(30000,45000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			else
				cloudlet = new Cloudlet(idShift++, newRandom(30000,45000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);			
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		return cvMap;
}

	private static Map<Cloudlet,Vm> createCloudlet_LR(int userId,int size, int idShift){
		// Creates a container to store Cloudlets
		Map<Cloudlet,Vm> cvMap = new HashMap<Cloudlet,Vm>();

		//cloudlet parameters
		long fileSize = 300;
		long outputSize = 300;
		int pesNumber = 1;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		Cloudlet cloudlet;

		int sXS = (int) (size * 0.20);
		int sS = (int) (size * 0.70);
		int sL = size - (sXS+sS);

		for(int i=0;i<sXS;i++){
				cloudlet = new Cloudlet(idShift++, newRandom(15000,45000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0;i<sS;i++){
				cloudlet = new Cloudlet(idShift++, newRandom(45000,150000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0;i<sL;i++){
				cloudlet = new Cloudlet(idShift++, newRandom(150000,900000) , pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		return cvMap;
}

	private static Map<Cloudlet,Vm> createCloudletRR(int userId,int size, int idShift){
		// Creates a container to store Cloudlets
		Map<Cloudlet,Vm> cvMap = new HashMap<Cloudlet,Vm>();

		//cloudlet parameters
//		long lengthXS = 20, lengthS = 200, length = 1000, lengthL = 7500, lengthXL = 10000;
		long fileSize = 300;
		long outputSize = 300;
		int pesNumber = 1;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		Cloudlet cloudlet;
		
		for(int i=0 ;i<size;i++){
			if (i%2==0)
				cloudlet = new Cloudlet(idShift++, newRandom(50,25000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			else
				cloudlet = new Cloudlet(idShift++, newRandom(50,25000), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);			
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		return cvMap;
}
	
	private static Map<Cloudlet,Vm> createCloudlet(int userId,int size, int idShift){
		// Creates a Map to store Cloudlets
		Map<Cloudlet,Vm> cvMap = new HashMap<Cloudlet,Vm>();
		
		//cloudlet parameters
		long lengthXS = 200, lengthS = 1000, length = 5000, lengthL = 15000, lengthXL = 45000;
		long fileSize = 300;
		long outputSize = 300;
		int pesNumber = 1;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		int cloudlets = 10000;
		//Cloudlet[] cloudlet = new Cloudlet[cloudlets];
		Cloudlet cloudlet;
		int sXS, sS, sM, sL, sXL;
		sXS = (int) (size * 0.05);
		sS = (int) (size * 0.15);
		sM = (int) (size * 0.05);
		sL = (int) (size * 0.35);
		sXL = size - (sXS+sS+sM+sL);
		
		for(int i=0;i<sXS;i++){
			cloudlet = new Cloudlet(idShift++, lengthXS, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sS;i++){
			cloudlet = new Cloudlet(idShift++, lengthS, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sM;i++){
			cloudlet = new Cloudlet(idShift++, length, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sL;i++){
			cloudlet = new Cloudlet(idShift++, lengthL, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);			
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sXL;i++){
			cloudlet = new Cloudlet(idShift++, lengthXL, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);			
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);

		}
		
		return cvMap;
	}

	private static Map<Cloudlet,Vm> createCloudletF(int userId,int size, int idShift){
		// Creates a Map to store Cloudlets
		Map<Cloudlet,Vm> cvMap = new HashMap<Cloudlet,Vm>();
		
		//cloudlet parameters
		long lengthXS = 200, lengthS = 1000, length = 5000, lengthL = 15000, lengthXL = 45000;
		long fileSize = 300;
		long outputSize = 300;
		int pesNumber = 1;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		int cloudlets = 10000;
		//Cloudlet[] cloudlet = new Cloudlet[cloudlets];
		Cloudlet cloudlet;
		int sXS, sS, sM, sL, sXL;
		sXS = (int) (size * 0.35);
		sS = (int) (size * 0.40);
		sM = (int) (size * 0.05);
		sL = (int) (size * 0.15);
		sXL = size - (sXS+sS+sM+sL);
		
		for(int i=0;i<sXS;i++){
			cloudlet = new Cloudlet(idShift++, lengthXS, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sS;i++){
			cloudlet = new Cloudlet(idShift++, lengthS, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sM;i++){
			cloudlet = new Cloudlet(idShift++, length, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sL;i++){
			cloudlet = new Cloudlet(idShift++, lengthL, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
			cloudlet.setUserId(userId);			
			cvMap.put(cloudlet,null);
		}
		for(int i=0 ;i<sXL;i++){
			cloudlet = new Cloudlet(idShift++, lengthXL, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);			
			cloudlet.setUserId(userId);
			cvMap.put(cloudlet,null);

		}
		
		return cvMap;
	}
	
	private static List<Vm> getVmList(Map<Vm,Double> vMap)
	{		
		Set<Vm> keySetVm = vmMap.keySet();
		List<Vm> vList = new LinkedList<Vm>(keySetVm);
		return vList;
	}
	
	private static List<Cloudlet> getCloudletList(Map<Cloudlet,Vm> cvMap)
	{		
		Set<Cloudlet> keySetCloudlet = cvMap.keySet();
		List<Cloudlet> cList = new LinkedList<Cloudlet>(keySetCloudlet);
		return cList;
	}

	private static Map<Vm,Double> setShareVmMap(List<Vm> vList, Long totalDemand)
	{
		Map<Vm,Double> vMap = new HashMap<Vm,Double>();
		Double powerFactor, vmShare;
		for (Vm vm:vList)
		{
		powerFactor = 0.0; 	vmShare = 0.0;		
		powerFactor = vm.getMips()/getTotalMIPS(vList);    // ratio of Vm share out of all available
		vmShare = totalDemand * powerFactor;
		vMap.put(vm, vmShare);
		}
		return vMap;
	}

	private static Map<Vm,Double> setExecTimeInVmMap(Map<Vm,Double> vMap, Cloudlet cloudlet, Vm vm)
	{		
		Double vmMakespan = vMap.get(vm);
		vMap.put(vm, getCloudletExecTime(cloudlet,vm,vmMakespan));	
		//Log.printLine("VM " + vm.getId() + "  execution time is set now: " + vMap.get(vm));
		return vMap;
	}

	private static double getCloudletExecTime(Cloudlet cloudlet, Vm vm, Double VmExecTime)
	{
		double execTime = 0.0;
		execTime = VmExecTime + (cloudlet.getCloudletLength()/vm.getMips());
		return execTime;
	}
	private static double getCloudletExecTime(Cloudlet cloudlet, Vm vm)
	{
		double execTime = 0.0;
		execTime = cloudlet.getCloudletLength()/vm.getMips();
		return execTime;
	}
	//Finding total MIPS of all available VMs
		private static Double getTotalMIPS(List<Vm> vList)
		{
			Double totalMIPS = 0.0;
			for(Vm vm:vList)
				totalMIPS += vm.getMips();
			return totalMIPS;
		}

	//method to update the execution share
	private static void updateVmShareMap(Map<Vm,Double> vmSMap, Cloudlet cloudlet, Vm vm)
	{
		Double vmShare=0.0;
		vmShare = vmSMap.get(vm) - cloudlet.getCloudletLength();
		vmMap = setExecTimeInVmMap(vmMap,cloudlet,vm);	      //Updating the finish time of VM
		vmSMap.put(vm, vmShare);
	}

	//Find the total length/size of all cloudlets need to be scheduled on cloud.
	private static long demandedCloudlets(List<Cloudlet> cList)
		{
			long cDemand = 0;
			for(int i=0;i<cList.size();i++)
				cDemand +=  cList.get(i).getCloudletLength();
			return cDemand;
		}

	private static void defineVmNCluodlet(int brokerID, int vm, int cloudlet)
	{
		vmMap = createVM(brokerID,vm,1);			
		vmList = getVmList(vmMap);	
		cloudlet2VmMap = createCloudletF(brokerID, cloudlet, 1);
		cloudletList = getCloudletList(cloudlet2VmMap);			
		vmShareMap = setShareVmMap(vmList,demandedCloudlets(cloudletList));
	}
	
	private static void checkPrinting()
	{
		Iterator<Cloudlet> cKeySetIterator = cloudlet2VmMap.keySet().iterator();
		while (cKeySetIterator.hasNext()){
		Cloudlet cld = cKeySetIterator.next();
		if (cloudlet2VmMap.get(cld)!=null)
			Log.printLine("Cloudlet " + cld.getCloudletId() + " has size " + cld.getCloudletLength() + " and assigned to VM = " + cloudlet2VmMap.get(cld).getId());
		else
			Log.printLine("Cloudlet " + cld.getCloudletId() + " has size " + cld.getCloudletLength() + " and assigned to VM = " + cloudlet2VmMap.get(cld));
		}
		for (Vm vm:vmList){
			Log.printLine("VM - " + vm.getId() + " has its share = " + vmShareMap.get(vm));
			}
	}
	
	private static double VmMakespane(List<Cloudlet> list, int VmId)
	{
		double mkspane = 0; 
		for(int i=0;i<list.size();i++)
			if (list.get(i).getVmId() == VmId)
				if (list.get(i).getFinishTime() > mkspane)
					mkspane =  list.get(i).getFinishTime(); 
		return mkspane;
	}
	
	//Find the VM with smallest remaining executing share
	private static Vm getMinVmShare(Map<Vm,Double> vSMap)
	{
		Vm min;
		Iterator<Vm> vmKeySetIterator = vSMap.keySet().iterator();
		min = vmKeySetIterator.next();			
		while (vmKeySetIterator.hasNext()){
			Vm vm = vmKeySetIterator.next();				
			if (vSMap.get(vm) < vSMap.get(min))
			min = vm;
		}
		return min;
	}
	
	// method to find the average waiting time in a virtual machine
	private static double VmAvgRt(List<Cloudlet> list, int VmId)
	{
		int c = 0;
		double art = 0; 
		for(int i=0;i<list.size();i++)
			if (list.get(i).getVmId() == VmId)
			{
				art = art + list.get(i).getExecStartTime();    c++;
			}
		if (art!=0)
			art =  art / c;
		return art;
	}

	//Finding all the cloudlets lesser than the share of VM that can be assigned  to this VM
	private static Cloudlet getMaxCloudlet4Vm(List<Cloudlet> cList, Vm vm)
	{
		List<Cloudlet> nList = new ArrayList<Cloudlet>();

		for (Cloudlet cld:cList)
			if (cld.getCloudletLength() < vmShareMap.get(vm) || cld.getCloudletLength() == vmShareMap.get(vm))
				nList.add(cld);
		
		Cloudlet maxCloudlet = null;
		
		if (nList.size()>0)
		maxCloudlet = getMaxCloudlet(nList);
			
		/*if (nList.size()>0)
			maxCloudlet = nList.get(0); 		
		for (Cloudlet ncld:nList)
			if (ncld.getCloudletLength() > maxCloudlet.getCloudletLength())				
			maxCloudlet = ncld;		
		*/
		return maxCloudlet;
	}
	
	private static Vm getVmWithMakeSpane(List<Cloudlet> cList, List<Vm> vList)
	{
		Map<Vm,Double> vMap = new HashMap<Vm,Double>();
		Double vmMakeSpane = 0.0, makeSpane = 0.0;
		Vm vm = null;
		for (int v=0; v < vList.size(); v++)
		{
		vmMakeSpane = getVmMakeSpane(cList,vList.get(v).getId());				
		if (vmMakeSpane > makeSpane)
			{
			makeSpane = vmMakeSpane; 
			vm = vList.get(v);
			}
		}
		return vm;
	}
	
	private static void assignCloudlet2Vm(List<Cloudlet> cList, Vm vm)
	{
		Boolean chk = true;
		do{
			Cloudlet cloudlet = getMaxCloudlet4Vm(cList, vm);
			if (cloudlet != null){
				cloudlet2VmMap.put(cloudlet, vm);
				updateVmShareMap(vmShareMap,cloudlet,vm);
				//checkPrinting();
				cloudletList.remove(cloudlet);
			}
			else
				chk = false;
			
		}while(chk);
	}

	//Find slowest VM in the VM list
	private static Vm getSlowVm(List<Vm> vmList)
	{
		Vm slowVm;
		slowVm = vmList.get(0);
		for (Vm vm:vmList){
			if (vm.getMips() < slowVm.getMips())
				slowVm = vm;
		}
		return slowVm;
	}
	
	//Find fastest VM in the VM list
	private static Vm getFastVm(List<Vm> vmList)
	{
		Vm fastVm;
		fastVm = vmList.get(0);
		for (Vm vm:vmList){
			if (fastVm.getMips() < vm.getMips())
				fastVm = vm;
		}
		return fastVm;
	}

	//Find the larger cloudlet out of whole list of cloudlet
	private static Cloudlet getMaxCloudlet(List<Cloudlet> cList)
		{
			Cloudlet maxCloudlet;
			maxCloudlet = cList.get(0);
			for(Cloudlet cld:cList){
				if (cld.getCloudletLength() > maxCloudlet.getCloudletLength())
					maxCloudlet = cld;
			}
			return maxCloudlet;
		}
	//Find the smallest cloudlet out of whole list of cloudlet
	private static Cloudlet getAvgCloudlet(List<Cloudlet> cList)
	{
		Cloudlet avgCloudlet; Double sum=0.0, avg = 0.0;
		for (Cloudlet cld:cList)
			sum += cld.getCloudletLength();
		avg = sum/cList.size();
		avgCloudlet = getMinCloudlet(cList);
		for(Cloudlet cld:cList){
			if (cld.getCloudletLength() > avgCloudlet.getCloudletLength() || cld.getCloudletLength() == avgCloudlet.getCloudletLength())
				avgCloudlet = cld;      break;
		}
		return avgCloudlet;
	}
	//Find the smallest cloudlet out of whole list of cloudlet
	private static Cloudlet getMinCloudlet(List<Cloudlet> cList)
		{
			Cloudlet minCloudlet;
			minCloudlet = cList.get(0);
			for(Cloudlet cld:cList){
				if (cld.getCloudletLength() < minCloudlet.getCloudletLength())
					minCloudlet = cld;
			}
			return minCloudlet;
		}

	private static double getVmMakeSpane(List<Cloudlet> list, int VmId)
	{
		double mkspane = 0; 
		for(int i=0;i<list.size();i++)
			if (list.get(i).getVmId() == VmId)
				if (list.get(i).getFinishTime() > mkspane)
					mkspane =  list.get(i).getFinishTime(); 
		return mkspane;
	}
		
	//Find the VM with largest remaining executing share
	private static Vm getMaxVmShare(Map<Vm,Double> vSMap)
	{
		Vm max;
		Iterator<Vm> vmKeySetIterator = vSMap.keySet().iterator();
		max = vmKeySetIterator.next();			
		while (vmKeySetIterator.hasNext()){
			Vm vm = vmKeySetIterator.next();
			if ( vSMap.get(vm) > vSMap.get(max))
				max = vm;
				}
		return max;
	}
	
	private static Double getAvgResourceUtilizationRatio(List<Vm> vList, List<Cloudlet> cList)
	{
		Double ARUR = 0.0, avgMakeSapne = 0.0, sum = 0.0;		
		for(Vm vm:vList){
		 sum += getVmMakeSpane(cList, vm.getId());
		}
		avgMakeSapne = sum/vList.size();
		ARUR = avgMakeSapne/getVmMakeSpane(cList,getVmWithMakeSpane(cList,vList).getId());
		return ARUR;
	}
	//Return Vm that has minimum ready time
	private static Vm getVmWithMinReadyTime(Map<Vm,Double> vMap)
	{
		Vm vm = getMaxVmShare(vmShareMap);
		for (int i=0; i<vmList.size(); i++)
			if (vmMap.get(vm) > vmMap.get(vmList.get(i)))
				vm = vmList.get(i);
		return vm;
	}

	private static Vm getVmWithMinFinishTime(Map<Vm,Double> vMap, Cloudlet cloudlet)
	{		
		Vm vm = getMaxVmShare(vmShareMap);
		Double execTime = getCloudletExecTime(cloudlet,vm,vmMap.get(vm));
		for (Vm v:vmList){			
			Double cExecTime = getCloudletExecTime(cloudlet,v,vmMap.get(v));
			if (execTime > cExecTime)
				vm = v;
		}
		return vm;
	}
	////////////////////////// STATIC METHODS ///////////////////////
	public static void main(String[] args) {
		Log.printLine("Starting CloudSimTestRALBA...");

		try {
			// First step: Initialize the CloudSim package. It should be called before creating any entities.
			int num_user = 1;   // number of cloud users
			Calendar calendar = Calendar.getInstance();
			boolean trace_flag = false;  // mean trace events
			CloudSim.init(num_user, calendar, trace_flag);			// Initialize the CloudSim library

			Instant start = Instant.now();
			
			
			//Datacenters are the resource providers in CloudSim. We need at list one of them to run a CloudSim simulation
			Datacenter datacenter0 = createDatacenter("Datacenter_0");
			DatacenterBroker broker = createBroker("Broker_0");
			int brokerId = broker.getId();
			Log.print("Broker   = " + broker.getId());
			noVm = 50;
			noCloudlet = 1000;
			//defineVmNCodlet(brokerId,noVm,noCloudlet);  // my define methode to set number of VM and Cloudlets
			//vmMap = createVM_LR(brokerId,noVm,1);
			vmMap = createVM(brokerId,noVm,1);
			vmList = getVmList(vmMap);	
			cloudlet2VmMap = createDataSet(brokerId,1);
			//cloudlet2VmMap = createCloudletRandom(brokerId, noCloudlet, 1);
			//cloudlet2VmMap = createCloudletF(brokerId, noCloudlet, 1);
			cloudletList = getCloudletList(cloudlet2VmMap);			
			vmShareMap = setShareVmMap(vmList,demandedCloudlets(cloudletList));
			broker.submitCloudletList(cloudletList);
			broker.submitVmList(vmList);
			
			//checkPrinting();
	
			int size1 = cloudletList.size();
			
			while (cloudletList.size() > 0) {   //  This point onward NEED to be modified 
			Vm vm = getMaxVmShare(vmShareMap);
			assignCloudlet2Vm(cloudletList,vm);
			if (getMinCloudlet(cloudletList).getCloudletLength() > vmShareMap.get(getMinVmShare(vmShareMap)))
				break;
			}
			Log.printLine("First round ended");
			
			int size = cloudletList.size();
			
			for(int i=0;i<size;i++){
				Cloudlet cloudlet = getMaxCloudlet(cloudletList);
				//Vm vm = getMaxVmShare(vmShareMap);
				Vm vm = getVmWithMinFinishTime(vmMap,cloudlet);
				cloudlet2VmMap.put( cloudlet, vm);
				updateVmShareMap(vmShareMap,cloudlet, vm);
				cloudletList.remove(cloudlet);
			}				

			Iterator<Cloudlet> cKeySetIterator = cloudlet2VmMap.keySet().iterator();			
			while (cKeySetIterator.hasNext())
			{
				//broker.bindCloudletToVm(cloudletList.get(c).getCloudletId(), vm.getId());
				Cloudlet cloudlet = cKeySetIterator.next();
				Log.printLine(cloudlet.getCloudletId() + "- Cloudlet  and  Vm = " + cloudlet2VmMap.get(cloudlet).getId());
//				if (cloudlet2VmMap.get(cloudlet) != null)
					broker.bindCloudletToVm(cloudlet.getCloudletId(), cloudlet2VmMap.get(cloudlet).getId());
			}
			
			vmList = getVmList(vmMap);			
			// Sixth step: Starts the simulation
			
			
			Instant end = Instant.now();
			
			CloudSim.startSimulation();
			// Final step: Print results when simulation is over
			List<Cloudlet> newList = broker.getCloudletReceivedList();

			CloudSim.stopSimulation();

//        	printCloudletList(newList);
        	
        	double sumAvgRt=0.00;
       	for (int a=0; a<noVm;a++){
        		Log.printLine("Average Response Time of Vm-" + vmList.get(a).getId() + "   =  " + VmAvgRt( newList, vmList.get(a).getId()));
        		sumAvgRt += VmAvgRt( newList, vmList.get(a).getId());
       	}
       	sumAvgRt /= noVm;
       	
        for (int a=0; a<noVm;a++){
        		Log.printLine("Makespane of Vm-" + vmList.get(a).getId() + "  with computing power " + vmList.get(a).getMips() + " is equal to  =  " + getVmMakeSpane( newList, vmList.get(a).getId()));
        	}

        Double makeSpane = getVmMakeSpane(newList, getVmWithMakeSpane(newList,vmList).getId());       	
        Log.printLine("Throughput = " + noCloudlet/makeSpane);
        Log.printLine("Average Response Time   =  " + sumAvgRt);       	
        Log.printLine("ARUR  = " + getAvgResourceUtilizationRatio(vmList,newList));
        Log.printLine("Makespane  ( VM- " +  getVmWithMakeSpane(newList, vmList).getId()  +" ) =  " + makeSpane);
        
			//Print the debt of each user to each datacenter
			datacenter0.printDebts();

			Log.printLine("Cloudlets by Spill Scheduler = " + size);
			Log.printLine("Cloudlets by Fill Scheduler = " + (size1-size));
			Log.printLine("CloudSimTestRALBA finished!");
			
			Duration timeElapsed = Duration.between(start, end);
			Log.printLine("Time taken: "+ timeElapsed.toMillis() +" milliseconds");
		
			
			}
		catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
		}
		
	}

	private static Datacenter createDatacenter(String name){

		// Here are the steps needed to create a PowerDatacenter:
				// 1. We need to create a list to store one or more
				//    Machines
				List<Host> hostList = new ArrayList<Host>();

				// 2. A Machine contains one or more PEs or CPUs/Cores. Therefore, should
				//    create a list to store these PEs before creating
				//    a Machine.
				List<Pe> peList1 = new ArrayList<Pe>();

				int mips = 4000;

				// 3. Create PEs and add these into the list.
				//for a quad-core machine, a list of 4 PEs is required:
				peList1.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList1.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList1.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList1.add(new Pe(3, new PeProvisionerSimple(mips)));

				//Another list, for a dual-core machine
				List<Pe> peList2 = new ArrayList<Pe>();

				peList2.add(new Pe(0, new PeProvisionerSimple(mips)));
				peList2.add(new Pe(1, new PeProvisionerSimple(mips)));

				List<Pe> peList3 = new ArrayList<Pe>();
				
				peList3.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList3.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList3.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList3.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList4 = new ArrayList<Pe>();

				peList4.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList4.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList4.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList4.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList5 = new ArrayList<Pe>();

				peList5.add(new Pe(0, new PeProvisionerSimple(mips)));
				peList5.add(new Pe(1, new PeProvisionerSimple(mips)));

				List<Pe> peList6 = new ArrayList<Pe>();

				peList6.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList6.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList6.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList6.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList7 = new ArrayList<Pe>();

				peList7.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList7.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList7.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList7.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList8 = new ArrayList<Pe>();

				peList8.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList8.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList8.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList8.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList9 = new ArrayList<Pe>();

				peList9.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList9.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList9.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList9.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList10 = new ArrayList<Pe>();

				peList10.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList10.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList10.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList10.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList11 = new ArrayList<Pe>();

				peList11.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList11.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList11.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList11.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList12 = new ArrayList<Pe>();

				peList12.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList12.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList12.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList12.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList13 = new ArrayList<Pe>();

				peList13.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList13.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList13.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList13.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList14 = new ArrayList<Pe>();

				peList14.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList14.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList14.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList14.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList15 = new ArrayList<Pe>();

				peList15.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList15.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList15.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList15.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList16 = new ArrayList<Pe>();

				peList16.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList16.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList16.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList16.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList17 = new ArrayList<Pe>();

				peList17.add(new Pe(0, new PeProvisionerSimple(mips)));
				peList17.add(new Pe(1, new PeProvisionerSimple(mips)));

				List<Pe> peList18 = new ArrayList<Pe>();

				peList18.add(new Pe(0, new PeProvisionerSimple(mips)));
				peList18.add(new Pe(1, new PeProvisionerSimple(mips)));

				List<Pe> peList19 = new ArrayList<Pe>();

				peList19.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList19.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList19.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList19.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList20 = new ArrayList<Pe>();

				peList20.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList20.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList20.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList20.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList21 = new ArrayList<Pe>();

				peList21.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList21.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList21.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList21.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList22 = new ArrayList<Pe>();

				peList22.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList22.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList22.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList22.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList23 = new ArrayList<Pe>();

				peList23.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList23.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList23.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList23.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList24 = new ArrayList<Pe>();

				peList24.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList24.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList24.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList24.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList25 = new ArrayList<Pe>();

				peList25.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList25.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList25.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList25.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList26 = new ArrayList<Pe>();

				peList26.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList26.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList26.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList26.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList27 = new ArrayList<Pe>();

				peList27.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList27.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList27.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList27.add(new Pe(3, new PeProvisionerSimple(mips)));

				List<Pe> peList28 = new ArrayList<Pe>();

				peList28.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList28.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList28.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList28.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList29 = new ArrayList<Pe>();

				peList29.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList29.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList29.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList29.add(new Pe(3, new PeProvisionerSimple(mips)));
				
				List<Pe> peList30 = new ArrayList<Pe>();

				peList30.add(new Pe(0, new PeProvisionerSimple(mips))); // need to store Pe id and MIPS Rating
				peList30.add(new Pe(1, new PeProvisionerSimple(mips)));
				peList30.add(new Pe(2, new PeProvisionerSimple(mips)));
				peList30.add(new Pe(3, new PeProvisionerSimple(mips)));
				//4. Create Hosts with its id and list of PEs and add them to the list of machines
						int hostId=0;
						int ram = 16384; //host memory (MB)
						long storage = 1000000; //host storage
						int bw = 10000;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList1,
		    				new VmSchedulerTimeShared(peList1)
		    			)
		    		); // This is our first machine

				hostId++;

				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList2,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Second machine
					
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList3,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Third machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList4,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Fourth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList5,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Fifth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList6,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Sixth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList7,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Seventh machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList8,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Eighth machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList9,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Nineth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList10,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Tenth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList11,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Eleventh machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList12,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twelveth machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList13,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Thirteenth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList14,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Fourteenth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList15,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Fifteenth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList16,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Sixteenth machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList17,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Seventeenth machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList18,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Eighteenth machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList19,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Nineteenth machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList20,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine
				
				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList21,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList22,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList23,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList24,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList25,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList26,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList27,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList28,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList29,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine

				hostId++;
				
				hostList.add(
		    			new Host(
		    				hostId,
		    				new RamProvisionerSimple(ram),
		    				new BwProvisionerSimple(bw),
		    				storage,
		    				peList30,
		    				new VmSchedulerTimeShared(peList2)
		    			)
		    		); // Twentith machine
				// 5. Create a DatacenterCharacteristics object that stores the
				//    properties of a data center: architecture, OS, list of
				//    Machines, allocation policy: time- or space-shared, time zone
				//    and its price (G$/Pe time unit).
				String arch = "x86";      // system architecture
				String os = "Linux";          // operating system
				String vmm = "Xen";
				double time_zone = 10.0;         // time zone this resource located
				double cost = 3.0;              // the cost of using processing in this resource
				double costPerMem = 0.05;		// the cost of using memory in this resource
				double costPerStorage = 0.1;	// the cost of using storage in this resource
				double costPerBw = 0.1;			// the cost of using bw in this resource
				LinkedList<Storage> storageList = new LinkedList<Storage>();	//we are not adding SAN devices by now

				DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
		                arch, os, vmm, hostList, time_zone, cost, costPerMem, costPerStorage, costPerBw);


				// 6. Finally, we need to create a PowerDatacenter object.
				Datacenter datacenter = null;
				try {
					datacenter = new Datacenter(name, characteristics, new VmAllocationPolicySimple(hostList), storageList, 0);
				} catch (Exception e) {
					e.printStackTrace();
				}

				return datacenter;
	}
	//We strongly encourage users to develop their own broker policies, to submit vms and cloudlets according
	//to the specific rules of the simulated scenario
	private static DatacenterBroker createBroker(String name){

		DatacenterBroker broker = null;
		try {
			broker = new DatacenterBroker(name);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return broker;
	}
	/**
	 * Prints the Cloudlet objects
	 * @param list  list of Cloudlets
	 */
	private static void printCloudletList(List<Cloudlet> list) {
		int size = list.size();
		Cloudlet cloudlet;

		String indent = "    ";
		Log.printLine();
		Log.printLine("========== OUTPUT ==========");
		Log.printLine("Cloudlet ID" + indent + "STATUS" + indent +
				"Data center ID" + indent + "VM ID" + indent + indent + "Time" + indent + "Start Time" + indent + "Finish Time");

		DecimalFormat dft = new DecimalFormat("###.##");
		for (int i = 0; i < size; i++) {
			cloudlet = list.get(i);
			Log.print(indent + cloudlet.getCloudletId() + indent + indent);

			if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS){
				Log.print("SUCCESS");

				Log.printLine( indent + indent + cloudlet.getResourceId() + indent + indent + indent + cloudlet.getVmId() +
						indent + indent + indent + dft.format(cloudlet.getActualCPUTime()) +
						indent + indent + dft.format(cloudlet.getExecStartTime())+ indent + indent + indent + dft.format(cloudlet.getFinishTime()));
			}
		}

	}
}
 
